export class Account {

    constructor(
        private _accountNum: number,
        private _balance: number,
        private _overdraft: number) { }

    deposit(amount: number) {
        if (this._overdraft > 0) {
            if (amount < this._overdraft) {
                this._overdraft -= amount;
            } else {
                this._balance += (amount - this._overdraft);
                this._overdraft = 0;
            }
        } else {
            this._balance += amount;
        }
    }

    withdraw(amount: number) {
        if (amount > this._balance) {
            this._overdraft += (amount - this._balance);
            this._balance = 0;
        } else {
            this._balance -= amount;
        }
    }

    get accountNum() {
        return this._accountNum;
    }

    set accountNum(accountNum: number) {
        this._accountNum = accountNum;
    }

    get balance() {
        return this._balance;
    }

    set balance(balance: number) {
        this._balance = balance;
    }

    get overdraft() {
        return this._overdraft;
    }

    set overdraft(overdraft: number) {
        this._overdraft = overdraft;
    }

}
