export interface Client {

    accounts: Array<number>,
    age: number,
    name: string
}
