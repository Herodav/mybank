import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserDataService } from '../services/user-data.service';
import { AuthService } from '../services/auth.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(
    private userdataService: UserDataService,
    private router: Router,
    private authService: AuthService) { }

  canLoad(route: import("@angular/router").Route,
    segments: import("@angular/router").UrlSegment[]): boolean | Observable<boolean> {

 
    if (!this.userdataService.isLoggedIn) {
      return this.authService.autoLogin().pipe(tap(
        (res) => {
          console.log('Auto-loging...', res);
          if (!res) {
            this.router.navigateByUrl('login');
          }
        }
      ));
    }
    return this.userdataService.isLoggedIn;
  }

}
