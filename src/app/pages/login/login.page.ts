import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, MenuController } from '@ionic/angular';
import {  Router } from '@angular/router';
import { Toast } from '@ionic-native/toast/ngx';  
import {emailValidator} from '../../validators/email-validator';
import { passwordValidator} from '../../validators/password-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginForm: FormGroup;
  login: User = { email: '', password: '' };
  submitted = false;
  errorMessage = '';

  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private router : Router,
    private toast: Toast,
    private menuCtrl: MenuController,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  createForm(){
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, emailValidator ] ],
      password: ['', [Validators.required, passwordValidator ]]
    });
  }

  onLogin(form: NgForm) {
    console.log('on login', this.login);
    this.submitted = true;

    if (form.valid) {
      this.presentLoading();
      this.authService.login(this.login.email, this.login.password)
        .subscribe(
          (res) => {
            this.loadingCtrl.dismiss();
            console.log('Logged in...');
            this.router.navigateByUrl('accounts');
          },
          (err) => {
            this.loadingCtrl.dismiss();
            this.errorMessage = this.authService.errorMessage;
            this.toast.show(`${this.errorMessage}`, '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          }
        );
    }
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...',
      spinner: 'bubbles',
      duration: 10000,
      keyboardClose: true
      
    });
    await loading.present();
  }
}
