import { Component, OnInit, Input } from '@angular/core';
import { Account } from 'src/app/models/account';
import { AccountService } from 'src/app/services/account.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-action-modal',
  templateUrl: './action-modal.page.html',
  styleUrls: ['./action-modal.page.scss'],
})
export class ActionModalPage implements OnInit {

  @Input() action: string;
  @Input() account: Account;
  public amount: number;
  public message: string;

  constructor(private modalCtrl: ModalController, private accountService: AccountService) {
  }

  ngOnInit() { }

  updateAccount() {
    if (!this.amount || this.amount < 10) {
      this.message = "Sorry, the amount should be greater than R10";
      return;
    }
    switch (this.action) {
      case 'withdraw':
        this.account.withdraw(this.amount);
        break;
      case 'deposit':
        this.account.deposit(this.amount);
        break;
      default:
        return;
    }
    this.accountService.updateAccount(this.account).subscribe(
      (res) => {
        console.log('updated...', res);
        this.dismiss();
      },
      (err) => {
        console.log('Failed updating...', err);
        this.dismiss();
      }
    )
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
