import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionModalPage } from './action-modal.page';

describe('ActionModalPage', () => {
  let component: ActionModalPage;
  let fixture: ComponentFixture<ActionModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
