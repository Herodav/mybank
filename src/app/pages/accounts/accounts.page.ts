import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { AccountService } from 'src/app/services/account.service';
import { ClientService } from 'src/app/services/client.service';
import { Client } from 'src/app/models/client';
import { Account } from 'src/app/models/account';
import { AlertController, ModalController, LoadingController, MenuController } from '@ionic/angular';
import { ActionModalPage } from '../action-modal/action-modal.page';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.page.html',
  styleUrls: ['./accounts.page.scss'],
})
export class AccountsPage implements OnInit {

  public client: Client;
  public account: Account;
  public action: string = null;
  public accounts$: Observable<Account[]>;

  constructor(
    private auth: AuthService,
    private accountsService: AccountService,
    private clientService: ClientService,
    private alertCtr: AlertController,
    private modalCtr: ModalController,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController) { }

  ngOnInit() {

    this.presentLoading();

    // get Client & populate accounts
    this.clientService.getClient()
      .subscribe(
        (res) => {
          this.client = res;
          console.log('Client:', this.client);
          this.accounts$ = new Observable<Account[]>();
          this.accounts$ = this.accountsService.getClientAccounts(this.client);
          this.loadingCtrl.dismiss();
        }
      );
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  onAccountSelected(account: Account) {
    this.account = account;
  }

  async showAlertSelectAction() {
    const alert = await this.alertCtr.create({
      header: 'Please select action...',
      buttons: [
        {
          text: 'Withdraw',
          handler: () => {
            this.action = 'withdraw';
            this.presentModal();
          }
        }, {
          text: 'Deposit',
          handler: () => {
            this.action = 'deposit';
            this.presentModal();
          }
        }
      ]
    });
    await alert.present();
  }

  async showAlertCreateAccount() {
    const alert = await this.alertCtr.create({
      header: 'Would you like to create a new account ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: () => { this.addAccount(); }
        }
      ]
    });
    await alert.present();
  }
  async presentModal() {
    const modal = await this.modalCtr.create({
      component: ActionModalPage,
      componentProps: {
        'action': this.action,
        'account': this.account
      }
    });
    return await modal.present();
  }

  addAccount() {
    this.accountsService.createAccountForClient(this.client)
      .subscribe(
        (res) => {
          console.log('Response is ', res);
          this.refresh();
        },
        (err) => { console.log('Unable to create account', err); }
      );
  }

  refresh() {
    this.presentLoading();
    this.accounts$ = this.accountsService.getClientAccounts(this.client);
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...',
      spinner: 'bubbles',
      duration: 3000

    });
    await loading.present();
  }

}
