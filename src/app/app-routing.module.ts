import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },  
  { path: 'accounts', loadChildren: './pages/accounts/accounts.module#AccountsPageModule', canLoad: [AuthGuard] },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'action-modal', loadChildren: './pages/action-modal/action-modal.module#ActionModalPageModule', canLoad: [AuthGuard] },
  { path: '**', redirectTo: 'login' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
