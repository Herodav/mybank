import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  storeData(key: string, data: string) {
    Plugins.Storage.set(
      {
        key: key,
        value: data
      }
    )
  }

  getData(key: string): Promise<{ value: string; }> {
    return Plugins.Storage.get({ key: key });
  }

  removeData(key: string) {
    return Plugins.Storage.get({ key: key });
  }

}
