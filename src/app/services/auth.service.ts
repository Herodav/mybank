import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserDataService } from './user-data.service';
import { StorageService } from './storage.service';
import { endpoints } from '../utils/endpoints';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public errorMessage = '';

  url = endpoints.loginUrl;

  constructor(private http: HttpClient,
    private userData: UserDataService,
    private storageService: StorageService) { }

  autoLogin() {
    return from(this.storageService.getData('userData')).pipe(map(
      (storedData) => {
        console.log('Stored data', storedData);
        if (!storedData || !storedData.value) {
          return null;
        }
        const data = JSON.parse(storedData.value) as { token: string, tokenExpDate: string, localId: string };
        const expirationTime = new Date(data.tokenExpDate);

        if (expirationTime <= new Date()) { //token has expired
          console.log('Token expired');
          this.userData.isLoggedIn = false;
          return null;
        }
        // retrieve stored data
        this.userData.localId = data.localId;
        this.userData.expirationTime = data.tokenExpDate;
        this.userData.token = data.token;
        return !!storedData;
      }
    ));
  }

  login(email: string, pwd: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    const body = {
      email: email,
      password: pwd,
      returnSecureToken: true
    }
    return this.http.post<any>(this.url, body, {
      headers: httpOptions.headers,
      observe: 'response'
    }).pipe(tap(
      (res) => {
        this.userData.token = res.body.idToken;
        this.userData.localId = res.body.localId;
        this.userData.expirationTime = new Date(new Date().getTime() + (+ res.body.expiresIn * 1000)).toISOString();

        // Store user data in local memory for auto-login
        const data = JSON.stringify({
          localId: this.userData.localId,
          token: this.userData.token,
          tokenExpDate: this.userData.expirationTime
        });
        this.storageService.storeData('userData', data);
      }
    ));
  }

  logout() {
    this.storageService.removeData('userData');
    this.userData.isLoggedIn = false;
    console.log('Logged out...')
  }
}
