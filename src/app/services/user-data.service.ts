import { Injectable } from '@angular/core';
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private _token: string = null;
  private _localId: string = null;
  private _expirationTime: string = null;
  private _isLoggedIn: boolean = false;

  constructor() { }

  set token(token: string) {
    this._token = token;
  }

  get token() {
    return this._token;
  }

  set localId(localId: string) {
    this._localId = localId;
  }

  get localId() {
    return this._localId;
  }

  set expirationTime(expirationTime: string) {
    this._expirationTime = expirationTime;
  }

  get expirationTime() {
    return this._expirationTime;
  }

  set isLoggedIn(isLoggedIn: boolean) {
    this._isLoggedIn = isLoggedIn;
  }

  get isLoggedIn() {
    return this._token != null &&
      this._localId != null &&
      this.expirationTime != null;
  }

}
