import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor } from '@angular/common/http';
import { HttpHandler, HttpRequest, HttpErrorResponse, HttpHeaders } from '@angular/common/http'

import { Observable, throwError } from 'rxjs';
import { UserDataService } from './user-data.service';
import { retry, catchError} from 'rxjs/operators';
import { AuthService } from './auth.service';


@Injectable()
export class AppInterceptor implements HttpInterceptor {


  constructor(private userData: UserDataService, private authService: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.userData.token) {
      const authReq = req.clone({
        headers: new HttpHeaders({ 'Content-Type': 'Application/JSON' })
      });
      console.log('Making an authorized request...');
      req = authReq;

    }
    return next.handle(req)
      .pipe(
        retry(1),
        catchError(
          (error: HttpErrorResponse) => {

            if (error.error instanceof ErrorEvent) {
              // client-side error
              this.authService.errorMessage = `Error: ${error.message}`;
              console.log('Error interceptor : ', this.authService.errorMessage);
            } else {
              // server-side error
              this.authService.errorMessage = error.error.error.message;
              console.log('Error : ', this.authService.errorMessage);

            }
            return throwError(this.authService.errorMessage);

          }
        )
      );
  }
}
