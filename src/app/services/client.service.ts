import { Injectable } from '@angular/core';
import { UserDataService } from './user-data.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { endpoints } from '../utils/endpoints';
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private userData: UserDataService, private http: HttpClient) { }


  getClient(): Observable<Client> {
    const url = `https://momentum-retail-practical-test.firebaseio.com/clients/${this.userData.localId}.json?auth=${this.userData.token}`;
    return this.http.get<Client>(url);
  }
}
