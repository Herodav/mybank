import { Injectable } from '@angular/core';
import { UserDataService } from './user-data.service';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Client } from '../models/client';
import { Account } from '../models/account';
import { endpoints } from '../utils/endpoints';

@Injectable({
  providedIn: 'root'
})
export class AccountService {


  constructor(
    private http: HttpClient, 
    private userData: UserDataService) { }

  private generateNewAccount(): Account {
    const accountNum = Math.floor((Math.random() * 1000000000) + 1);
    return new Account(accountNum, 0, 0);
  }

  private updateClientAccountList(client: Client): Observable<Array<any>> {
    const url = `${endpoints.clientsUrl}${this.userData.localId}/accounts.json?auth=${this.userData.token}`;
    console.log('list updated...', client);
    return this.http.put<any>(url, client.accounts);
  }

  getAccount(accountNum: number): Observable<any> {
    const url = `https://momentum-retail-practical-test.firebaseio.com/accounts/${accountNum}.json?auth=${this.userData.token}`;
    return this.http.get<any>(url);
  }

  updateAccount(account: Account): Observable<any> {
    const url = `https://momentum-retail-practical-test.firebaseio.com/accounts/${account.accountNum}.json?auth=${this.userData.token}`;
    const body = {
      balance: account.balance,
      overdraft: account.overdraft
    };
    return this.http.put<any>(url, body);
  }

  createAccountForClient(client: Client): Observable<any>{
    const account = this.generateNewAccount();
    client.accounts.push(account.accountNum);
    const url = `${endpoints.accountsUrl}${account.accountNum}.json?auth=${this.userData.token}`

    const body = {
      balance: account.balance,
      overdraft: account.overdraft
    }
    this.updateClientAccountList(client).subscribe();
    console.log('List updated...', account  );
    return this.http.put<any>(url, body);
  }

  getClientAccounts(client: Client): Observable<Account[]> {
    const accountNums = client.accounts;
    const accountsList: Account[] = [];
    const accounts$: Subject<Account[]> = new Subject<Account[]>();

    accountNums.forEach((accountNum) => {
      this.getAccount(accountNum).subscribe(
        (res) => {
          if (res) {
            const newAccount = new Account(accountNum, res.balance, res.overdraft);
            accountsList.push(newAccount);
            accounts$.next(accountsList);
            console.log('Added one account: ', newAccount);
          }
        });
    });
    return accounts$.asObservable();
  }
}
