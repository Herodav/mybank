import { AbstractControl } from '@angular/forms';


export function passwordValidator(control: AbstractControl): { [key: string]: any } | null {

    const password = new RegExp('(?=.{8,})');
    const valid = password.test(control.value);

    return valid
        ? null
        : { invalidEmail: { valid: false, value: control.value } }
}

