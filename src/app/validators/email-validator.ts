import { AbstractControl } from '@angular/forms';


export function emailValidator(control: AbstractControl): { [key: string]: any } | null {

    const emailPattern = new RegExp('^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$');
    const valid = emailPattern.test(control.value);

    return valid
        ? null
        : { invalidEmail: { valid: false, value: control.value } }
}

